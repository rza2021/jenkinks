import pytest
from playwright.sync_api import Playwright, sync_playwright
from allure_pytest import plugin

@pytest.fixture(scope="session")
def playwright_instance():
    with sync_playwright() as playwright:
        yield playwright

@pytest.fixture(scope="session")
def browser(playwright_instance: Playwright):
    browser = playwright_instance.chromium.launch()
    yield browser
    browser.close()

@pytest.fixture(scope="function")
def page(browser):
    context = browser.new_context()
    page = context.new_page()
    yield page
    page.close()
    context.close()

def pytest_configure(config):
    # Убедитесь, что Allure репорты будут сохраняться в нужный каталог
    config.option.allure_report_dir = 'allure-results'
