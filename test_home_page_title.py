import pytest
import allure

@allure.feature('Главная страница')
@allure.story('Проверка заголовка')
def test_home_page_title(page):
    page.goto("https://example.com")
    assert page.title() == "Example Domain"